 
## partition 
``` bash
# create partion type linux
fdisk /dev/sda
```

## filesystem
``` bash
mkfs.btrfs /dev/sda1
# btrfs-progs v5.10.1
# See http://btrfs.wiki.kernel.org for more information.
# 
# Detected a SSD, turning off metadata duplication.  Mkfs with -m dup if you want # to force metadata duplication.
# Label:              (null)
# UUID:               e1624d19-3a0f-4d30-bc3d-e432ddf00819
# Node size:          16384
# Sector size:        4096
# Filesystem size:    931.51GiB
# Block group profiles:
#   Data:             single            8.00MiB
#   Metadata:         single            8.00MiB
#   System:           single            4.00MiB
# SSD detected:       yes
# Incompat features:  extref, skinny-metadata
# Runtime features:
# Checksum:           crc32c
# Number of devices:  1
# Devices:
#    ID        SIZE  PATH
#     1   931.51GiB  /dev/sda1
```

## /etc/fstab
``` 
UUID=e1624d19-3a0f-4d30-bc3d-e432ddf00819 /mnt/ssd1to btrfs defaults 0 1
```