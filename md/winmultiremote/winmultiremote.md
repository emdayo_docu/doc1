# win11 multi-remote

source : https://pupuweb.com/solved-fix-multiple-rdp-sessions-connections-windows10-windows11/

## étape 1
``` powershell
# élévation de droit en administrateur
c:
cd \Windows\System32
copy termsrv.dll termsrv.dll_backup
```
## étape 2
``` powershell
# changement propriétaire
takeown /F c:\Windows\System32\termsrv.dll /A

# -> Opération réussie : le fichier (ou dossier) : "c:\Windows\System32\termsrv.dll" appartient désormais au groupe d’administrateurs.
```

## étape 3
``` powershell
# 
icacls c:\Windows\System32\termsrv.dll /grant Administrators:F
```
**si échec** :

![alt](2022-12-28%2012_11_25-Autorisations%20pour%20termsrv.dll.png)

## étape 4
``` powershell
Net stop TermService
```
**alternative** :
![alt](Capture%20d'%C3%A9cran_20221228_131422.png)

## étape 5
``` powershell
Get-ComputerInfo | select WindowsProductName, WindowsVersion
# -> WindowsProductName WindowsVersion
# -> ------------------ --------------
# -> Windows 10 Pro     2009
```
## étape 6

récupérer un éditeur héxadécimal

## étape 7

éditer termsrv.dll

![alt](2022-12-28%2013_34_41-C__Windows_System32_termsrv.dll%20-%20Notepad%2B%2B%20%5BAdministrator%5D.png)

## étape 8
```
39 81 ac 43 20 20 75 29 48 8b 89 48
39 81 ac 43 00 00 75 29 48 8b 89 48
devient
B8 00 01 00 00 89 81 38 06 00 00 90
```
## étape 9

sauvegarder le fichier puis lancer le service termsrv

``` powershell
Net start TermService
```