# Logiciels alternatifs

|Windows |Linux |
|---|---|
| Ms Office 		|Libre Office|
| Adobe After Effect|Natron / ButtleOFX|
| Adobe Premiere	|OpenShot|
| Adobe Photoshop	|Gimp|
| Adobe Lightroom	|Raw Therapee / Darktable|
| Adobe Illustrator	|Inkscape|
| Adobe Acrobate	|Master PDF|
| Adobe Animate		|Synfig Studio|
| Adobe Audition	|Ardour|
| Adobe InDesign	|Scribus|
| 3dsMax		    |Blender|
| AutoCAD		    |FreeCAD|
